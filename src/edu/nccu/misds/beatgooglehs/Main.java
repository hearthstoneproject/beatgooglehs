package edu.nccu.misds.beatgooglehs;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8"); // print  
        response.setContentType("text/html; charset=UTF-8");        
        PrintWriter out = response.getWriter();
        String keyword = new String(request.getParameter("datafromtestFile").getBytes("ISO-8859-1"), "UTF-8");
        keyword = URLEncoder.encode(keyword, "UTF-8");
        Run hope = new Run();
        hope.fullky();
        ArrayList<Website> results = hope.printresult(keyword);
        for (Website page : results) {
        	if(page.globalScore > 0) {
        	String url = "href='" + page.urlStr + "'"; // ""  "href="'url'"     <a href='page.ulStr'><p >page.title (page.globalScore)</p><br></a>
        	out.println("<a " + url + " " + "target='_blank'" + ">" + "<p " + "style='margin-left:417px;'" + ">" + page.title + " " + "(" + page.globalScore + ")" +  "</p>" +"<br>" + "</a>");
        	}
        }
        out.flush();
        out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
