package edu.nccu.misds.beatgooglehs;

import java.io.IOException;
import java.util.ArrayList;


public class Run {
	public ArrayList<Keyword> keywords = new ArrayList<>();
	
	public void fullky() throws IOException {
		keywords.add(new Keyword("法師", 3));
		keywords.add(new Keyword("獵人", 3));
		keywords.add(new Keyword("戰士", 3));
		keywords.add(new Keyword("聖騎", 3));
		keywords.add(new Keyword("牧師", 3));
		keywords.add(new Keyword("盜賊", 3));
		keywords.add(new Keyword("德魯伊", 3));
		keywords.add(new Keyword("薩滿", 3));
		keywords.add(new Keyword("術士", 3));
		keywords.add(new Keyword("基本", 2));
		keywords.add(new Keyword("普通", 2));
		keywords.add(new Keyword("精良", 2));
		keywords.add(new Keyword("史詩", 2));
		keywords.add(new Keyword("傳說", 2));
	}

	public ArrayList<Website> printresult(String searchword) throws IOException {
		BeatGoogle beatGoogle = new BeatGoogle(keywords);
		String implicit = "+爐石";
		ArrayList<Website> resultWebs = beatGoogle.query(searchword + implicit);
		return resultWebs;
	}
}